<?php
/**
 * Listable Child functions and definitions
 *
 * Bellow you will find several ways to tackle the enqueue of static resources/files
 * It depends on the amount of customization you want to do
 * If you either wish to simply overwrite/add some CSS rules or JS code
 * Or if you want to replace certain files from the parent with your own (like style.css or main.js)
 *
 * @package ListableChild
 */

/**
 * Setup Listable Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be filed in the /languages/ directory.
 */
function listable_child_theme_setup() {
	load_child_theme_textdomain( 'listable-child-theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'listable_child_theme_setup' );

/**
 *
 * 1. Add a Child Theme "style.css" file
 * ----------------------------------------------------------------------------
 *
 * If you want to add static resources files from the child theme, use the
 * example function written below.
 */
function listable_child_enqueue_styles() {
	$theme = wp_get_theme();
	// Use the parent version for cachebusting.
	$parent = $theme->parent();

	if ( is_rtl() ) {
		wp_enqueue_style( 'listable-rtl-style', get_template_directory_uri() . '/rtl.css', array(), $parent->get( 'Version' ) );
	} else {
		wp_enqueue_style( 'listable-main-style', get_template_directory_uri() . '/style.css', array(), $parent->get( 'Version' ) );
	}

	/**
	 * Here we are adding the child style.css while still retaining
	 * all of the parents assets (style.css, JS files, etc).
	 */
	wp_enqueue_style( 'listable-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'listable-style' ) // Make sure the the child's style.css comes after the parents so you can overwrite rules.
	);
}
add_action( 'wp_enqueue_scripts', 'listable_child_enqueue_styles' );

/**
 *
 * 2. Overwrite Static Resources (eg. style.css or main.js)
 * ----------------------------------------------------------------------------
 *
 * If you want to overwrite static resources files from the parent theme
 * and use only the ones from the Child Theme, this is the way to do it.
 */
function listable_child_overwrite_files() {

	// 1. The "main.js" file
	//
	// Let's assume you want to completely overwrite the "main.js" file from the parent
	// First you will have to make sure the parent's file is not loaded
	// See the parent's function.php -> the listable_scripts_styles() function
	// for details like resources names
	wp_dequeue_script( 'listable-scripts' );

	// We will add the main.js from the child theme (located in assets/js/main.js)
	// with the same dependecies as the main.js in the parent
	// This is not required, but I assume you are not modifying that much :).
	wp_enqueue_script( 'listable-child-scripts',
		get_stylesheet_directory_uri() . '/assets/js/main.js',
		array( 'jquery' ),
	'1.0.0', true );

	// 2. The "style.css" file
	//
	// First, remove the parent style files
	// see the parent's function.php -> the hive_scripts_styles() function for details like resources names
	wp_dequeue_style( 'listable-style' );

	// Now you can add your own, modified version of the "style.css" file.
	wp_enqueue_style( 'listable-child-style',
		get_stylesheet_directory_uri() . '/style.css'
	);
}

/**
 * Load the files from the function mentioned above.
 *
 * Notes:
 * - The 11 priority parameter is need so we do this after the function
 *   in the parent so there is something to dequeue
 * - The default priority of any action is 10
 */
add_action( 'wp_enqueue_scripts', 'listable_child_overwrite_files', 11 );


/**
 * Code added by me (Gizan)
 */

/**
 * Adjust the phone field to NL
 *
 * @author Gizan
 * @param  Array $args Arguments.
 * @return Array Altered arguments
 */
function my_custom_phone_args( $args ) {
	$args['preferredCountries'] = array( 'be', 'de','fr' );
	$args['defaultCountry'] = 'auto';
	return $args;
}
add_filter( 'job_manager_field_editor_phone_args', 'my_custom_phone_args' );

/**
 * Add a new vendor when creating a listing
 *
 * @author Nabil Kadimi <nabil@kadimi.com>
 * @author Gizan
 * @param Integer $user_id The user ID.
 */
function add_new_vendor( $post_ID, $post, $update ) {

	/**
	 * Only if post is a `job_listing`.
	 */
	if ( 'job_listing' !== $post->post_type ) {
		return;
	}

	/**
	 * Exit function if user is not an `employer`.
	 */
	if ( ! current_user_can( 'employer' ) ) {
		return;
	}

	/**
	 * Create vendor.
	 */
	$current_user   = wp_get_current_user();
	$vendor_name = sprintf('%s (%d)', $current_user->user_login, $current_user->ID ); // Example: Gizan (45).
	$vendor_term = term_exists( $vendor_name, 'wcpv_product_vendors' );
	if ( ! $vendor_term ) {
		$vendor_term = wp_insert_term( $vendor_name, 'wcpv_product_vendors' );
	}

	/**
	 * Update vendor.
	 */
	$vendor_data = array(
		// 'paypal'  => '',
		'profile' => $current_user->user_email,
		'email'   => $current_user->user_email,
	);
	update_term_meta( $vendor_term['term_id'], 'vendor_data', $vendor_data );

	add_new_product( $post->ID );
}
// add_action( 'wp_insert_post', 'add_new_vendor',1, 3 );

/**
 * Create the product.
 *
 * @param Integer $ad_id The ad post ID.
 * @author Gizan
 */
function add_new_product( $ad_id ) {

	/**
	 * First see if product already exist or is updated.
	 */
	$current_user = wp_get_current_user();
	$product_titel = 'Wasservice van ' . $current_user->nickname;
	$args = array(
		'post_type'      => 'product',
		// 'posts_per_page' => 30000,
		'title'          => $product_titel,
	);
	$posts_array = get_posts( $args );

	$product_update = (bool) $posts_array;

	/**
	 * If product exist, update it, otherwise create it.
	 */
	if ( $product_update ) {
		// echo 'Dit is een update';
	} else {
		$post_id = wp_insert_post( array(
			'post_type' => 'product',
			'post_title' => 'Wasservice van ' . $current_user->nickname,
			'post_content' => 'Dit is de wasservice van ' . $current_user->nickname,
			'post_status' => 'publish',
			'comment_status' => 'closed',   // if you prefer
			'ping_status' => 'closed',      // if you prefer
		) );
		wp_set_object_terms( $post_id,'booking','product_type' );

		// update_post_meta( $post_id, '_wc_booking_has_persons' , true );
		// update_post_meta( $post_id, '_wc_booking_has_person_types' , 'yes' );

		$meta = array(
			'_wc_booking_has_persons'               => 'no',
			'_wc_booking_min_duration'              => '1',
			'_wc_booking_max_duration'              => '1',
			'_wc_booking_calendar_display_mode'     => 'always_visible',
			'_wc_booking_qty'                       => '100',
			'_wc_booking_person_qty_multiplier'     => 'no',
			'_wc_booking_person_cost_multiplier'    => 'no',
			'_wc_booking_min_persons_group'         => '1',
			'_wc_booking_max_persons_group'         => '70',
			'_wc_booking_has_person_types'          => 'no',
			// '_wc_booking_has_resources'             => 'yes',
			// '_wc_booking_resouce_label'             => 'Bezorging',
			// '_wc_booking_resources_assignment'      => 'customer',
			'_wc_booking_duration_type'             => 'fixed',
			'_wc_booking_duration'                  => '1',
			'_wc_booking_cost'                      => '5',
			'_price'                                => '10',
			'_wc_booking_base_cost'                 => '2',
			'_wc_booking_duration_unit'             => 'hour',
			'_wc_booking_user_can_cancel'           => 'yes',
			'_wc_booking_cancel_limit'              => '1',
			'_wc_booking_cancel_limit_unit'         => 'month',
			'_wc_booking_max_date'                  => '12',
			'_wc_booking_max_date_unit'             => 'month',
			'_wc_booking_min_date_unit'             => 'month',
			'_wc_booking_first_block_time'          => '09:00',
			'_wc_booking_requires_confirmation'     => 'no',
			'_wc_booking_default_date_availability' => 'available',
			'_has_additional_costs'                 => 'yes',
			'_product_image_gallery'                => '11155',
			'_thumbnail_id'                         => '11155',
		);
		foreach ( $meta as $key => $value ) {
			add_post_meta( $post_id, $key, $value );
		}

		/**
		 * @todo Do we need this?
		 */
		$meta_to_save = array(
			'11213' => '5',
			'11214' => '5',
			'11275' => '5',
			'11277' => '5',
		);

		$person_type_var = array();
		$person_type_var[1]['person_id']         = '14987';
		$person_type_var[1]['person_name']       = 'test';
		$person_type_var[1]['person_cost']       = '3';
		$person_type_var[1]['person_block_cost'] = '3';
		$person_type_var[1]['person_min']        = '0';
		$person_type_var[1]['person_max']        = '10';

		/**
		 * @todo Fix this code, `$product_ID` is not definded
		 */
		// add_post_meta( $product_ID, '_wc_booking_has_person_types', $person_type_var );

		/**
		 * Add product to listing.
		 */
		update_post_meta( $post_id, '_products', array( $ad_id ) );
	}
}

// Disable i18n.
add_action( 'gettext', function( $a, $b, $c ) { return $b; }, 10, 3 );
